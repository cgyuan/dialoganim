package com.example.dialoganim;

import com.mstar.android.tv.TvChannelManager;
import com.nineoldandroids.view.ViewHelper;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {
	
	private NotifyDialog mDialog;
	private View mContentView;
	private TvChannelManager tv;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		mDialog = new NotifyDialog(this, R.style.dialog_untran);
		
		mContentView = View.inflate(this, R.layout.dialog_layout, null);
		mContentView.findViewById(R.id.button1).setOnClickListener(new OnDialogButtonClickListener());
		mContentView.findViewById(R.id.button2).setOnClickListener(new OnDialogButtonClickListener());
		mDialog.setContentView(mContentView);
		
		mDialog.setOnShowListener(new OnShowListener() {
			
			@Override
			public void onShow(DialogInterface dialog) {
				
			}
		});
	}
	
	private class OnDialogButtonClickListener implements View.OnClickListener{

		@Override
		public void onClick(View v) {
			removeDialog();
			switch (v.getId()) {
			case R.id.button1:
				
				break;

			case R.id.button2:
				
				break;
			}
		}
		
	}
	
	public void click(View v){
		mDialog.show();
		reset(mContentView);
		AnimatorSet set = new AnimatorSet();
		switch (v.getId()) {
		case R.id.id_slide_top:
			set.playTogether(
					ObjectAnimator.ofFloat(mContentView, "translationY", -800, 0).setDuration(700),
	                ObjectAnimator.ofFloat(mContentView, "alpha", 0, 1).setDuration(700*3/2));
			set.start();
			break;
			
		case R.id.id_slide_left:
			set.playTogether(
					ObjectAnimator.ofFloat(mContentView, "translationX", -300, 0).setDuration(700),
	                ObjectAnimator.ofFloat(mContentView, "alpha", 0, 1).setDuration(700*3/2));
			set.start();
			break;

		case R.id.id_rotate:
			set.playTogether(
					ObjectAnimator.ofFloat(mContentView, "rotationY", 90, 0).setDuration(700),
	                ObjectAnimator.ofFloat(mContentView, "translationX", -300, 0).setDuration(700),
	                ObjectAnimator.ofFloat(mContentView, "alpha", 0, 1).setDuration(700*3/2));
			set.start();
			break;
			
		case R.id.id_shake:
			set.playTogether(ObjectAnimator.ofFloat(mContentView, "translationX", 0, .10f, -25, .26f, 25,.42f, -25, .58f, 25,.74f,-25,.90f,1,0).setDuration(700));
			set.start();
			break;
		}
	}
	
	public void reset(View view) {
        ViewHelper.setPivotX(view, view.getMeasuredWidth() / 2.0f);
        ViewHelper.setPivotY(view, view.getMeasuredHeight() / 2.0f);
    }
	
	private void removeDialog(){
		if(mDialog != null && mDialog.isShowing()){
			mDialog.dismiss();
		}
	}
}
